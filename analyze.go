package main

import (
	"io"
	"os"
	"os/exec"
	"path/filepath"

	"github.com/urfave/cli"
)

const flagCompile = "compile"
const flagMavenCliOpts = "mavenCliOpts"

func analyzeFlags() []cli.Flag {
	return []cli.Flag{
		cli.BoolTFlag{
			Name:  flagCompile,
			Usage: "Compile Java code. It's not needed if the code is already compiled.",
		},
		cli.StringFlag{
			Name:   "mavenCliOpts",
			Value:  "",
			Usage:  "Optional arguments for the maven CLI",
			EnvVar: "MAVEN_CLI_OPTS",
		},
	}
}

const (
	pathBash          = "/bin/bash"
	pathMvn           = "/usr/share/maven/bin/mvn"
	pathFindSecBugJar = "/findsecbugs-plugin.jar"
	pathIncludeXML    = "/include.xml"
	pathSpotBugsXML   = "./target/spotbugsXml.xml"
)

func analyze(c *cli.Context, path string) (io.ReadCloser, error) {
	var setupCmd = func(cmd *exec.Cmd) *exec.Cmd {
		cmd.Dir = path
		cmd.Env = os.Environ()
		cmd.Stdout = os.Stdout
		cmd.Stderr = os.Stderr
		return cmd
	}

	// compile Java source code if needed
	if c.BoolT(flagCompile) {
		err := setupCmd(exec.Command(pathBash, pathMvn, c.String(flagMavenCliOpts), "compile")).Run()
		if err != nil {
			return nil, err
		}

	}
	// run spotbugs with findsecbugs
	// mvn com.github.spotbugs:spotbugs-maven-plugin:spotbugs --non-recursive -Dspotbugs.includeFilterFile=/include.xml -Dspotbugs.pluginList="/findsecbugs-plugin.jar"
	err := setupCmd(exec.Command(pathBash, pathMvn,
		c.String(flagMavenCliOpts),
		"com.github.spotbugs:spotbugs-maven-plugin:spotbugs",
		"--non-recursive",
		"-Dspotbugs.includeFilterFile="+pathIncludeXML,
		"-Dspotbugs.pluginList="+pathFindSecBugJar)).Run()
	if err != nil {
		return nil, err
	}

	return os.Open(filepath.Join(path, pathSpotBugsXML))
}
