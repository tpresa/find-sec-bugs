# find-sec-bugs analyzer changelog

## v1.4.0
- Upgrade to Maven 3.6, JDK 8 and SpotBugs Maven Plugin 3.1.10
- Fix Maven build failure: "Source option 5 is no longer supported."

## v1.3.0
- Add `Scanner` property and deprecate `Tool`

## v1.2.0
- Show command error output

## v1.1.0
- Enrich report with more data

## v1.0.0
- Rewrite using Go and analyzers common library

## v0.1.0
- initial release
